// Server

const express = require("express");
const cors = require("cors");
const axios = require("axios");
const qr = require("./queryRunner.js");

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
const port = 4000;

app.use(cors());

app.post("/launches", async (req, res) => {
  let data = await qr.launches(req);
  res.send(data.data);
});

app.get("/launches/id", async (req, res) => {
  let data = await qr.launchesById(req);
  res.send(data.data);
});

app.get("/rockets", async (req, res) => {
  let data = await qr.rockets();
  res.send(data.data);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
