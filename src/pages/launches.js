import { useEffect, useState } from "react";
import CustomModal from "../components/CustomModal";
import LaunchCard from "../components/lauch-card";
import Pagination from "../components/Pagination";
import Paragraph from "../components/paragraph";
import Search from "../components/search";
import Title from "../components/title";
import Video from "../components/video";
import { LAUNCHES, LAUNCHES_BY_ID } from "../constants/endpoints";
import apiCaller from "../interceptors/apiCaller";
import ContentSelector from "../layout/contentSelector";
import Grid from "../layout/grid";
import Section from "../layout/section";
import Wrapper from "../layout/wrapper";

const Launches = () => {
  const [launches, setLaunches] = useState([]);
  const [page, setPaging] = useState("");
  const [missionName, setMissionName] = useState("");
  const [loading, setLoading] = useState(true);
  const [openModal, setOpenModal] = useState(false);
  const [launchDetails, setLaunchDetails] = useState("");
  const fetchData = async (payload) => {
    setLoading(true);
    const result = await apiCaller.post(LAUNCHES, payload);
    const { docs, page, totalDocs, totalPages, limit } = result.data;
    setLaunches(docs);
    setPaging({ page, totalDocs, totalPages, limit });
    setLoading(false);
  };

  const fetchOne = async (payload) => {
    const result = await apiCaller.get(LAUNCHES_BY_ID, {
      params: { id: payload },
    });

    setLaunchDetails(result.data);
    setLoading(false);
  };

  const inputHandler = (e) => {
    e.target.value === "" && fetchData();
    setMissionName(e.target.value);
  };

  const searchHandler = () => {
    let params = {
      query: { ...getFilter() },
      options: {},
    };
    fetchData(params);
  };

  const pageHandler = (key, value) => {
    console.log(key, value);
    let params = {
      query: { ...getFilter() },
      options: { ...page, [key]: value },
    };
    setPaging({ ...page, [key]: value });
    fetchData(params);
  };

  const getFilter = () => {
    let filter = "";
    if (missionName) {
      filter = {
        name: missionName,
      };
    }
    return filter;
  };

  useEffect(() => {
    fetchData();
  }, []);

  const viewMore = (e) => {
    if (e.target.name == "viewMore") {
      setLaunchDetails("");
      fetchOne(e.target.id);
      toggleModal();
    }
  };

  const toggleModal = () => {
    setOpenModal(!openModal);
  };

  return (
    <Section>
      {loading && <div>loading....</div>}

      {!loading && (
        <Wrapper>
          <Title tag="h3" title="Launches" />
          <ContentSelector>
            <Search
              placeHolder="Mission Name"
              inputHandler={inputHandler}
              clickHandler={searchHandler}
              value={missionName}
            />
          </ContentSelector>
          <Grid handler={viewMore}>
            {launches?.map((item, index) => (
              <LaunchCard
                key={index.toString()}
                id={item.id}
                image={item.links.patch.small}
                title={item.name}
                description={item.details}
                viewMore
              />
            ))}
          </Grid>
          <hr />

          <Pagination pageDetails={page} pageHandler={pageHandler} />
          <CustomModal
            title={launchDetails.name}
            open={openModal}
            toggleModal={toggleModal}
          >
            <Video videoId={launchDetails?.links?.youtube_id} />
            <Paragraph text={launchDetails.details} />
          </CustomModal>
        </Wrapper>
      )}
    </Section>
  );
};

export default Launches;
