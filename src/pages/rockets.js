import { useEffect, useState } from "react";
import LaunchCard from "../components/lauch-card";
import { ROCKETS } from "../constants/endpoints";
import apiCaller from "../interceptors/apiCaller";
import Grid from "../layout/grid";
import Section from "../layout/section";
import Wrapper from "../layout/wrapper";
import Title from "../components/title";

const Rockets = () => {
  const [data, setData] = useState({ rockets: [] });
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const result = await apiCaller.get(ROCKETS);
      setData({ rockets: result.data });
      setLoading(false);
    };
    fetchData();
  }, []);

  return (
    <Section>
      {loading && <div>loading....</div>}
      {!loading && (
        <Wrapper>
          <Title tag="h3" title="Rockets" />
          <Grid>
            {data.rockets.map((item, index) => (
              <LaunchCard
                key={index.toString()}
                image={item.flickr_images[0]}
                title={item.name}
                description={item.description}
              />
            ))}
          </Grid>
        </Wrapper>
      )}
    </Section>
  );
};

export default Rockets;
