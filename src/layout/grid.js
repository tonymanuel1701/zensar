import styled from "styled-components";

const GridWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

const grid = ({ children, handler }) => (
  <GridWrapper onClick={handler}>{children}</GridWrapper>
);

export default grid;
