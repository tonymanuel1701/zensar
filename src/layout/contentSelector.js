import styled from "styled-components";

const ContentWrapper = styled.div`
  width: 100%;
  margin: 20px auto;
  display: flex;
  justify-content: flex-end;

  button {
    border: none;
    padding: 10px;
    min-width: 100px;
    margin-right: 10px;
  }
`;

const ContentSelector = ({ children }) => (
  <ContentWrapper>{children}</ContentWrapper>
);

export default ContentSelector;
