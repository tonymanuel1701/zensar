import PropTypes from "prop-types";
import React, { useState } from "react";
import styled from "styled-components";
import { device } from "../../helpers";
import Button from "../button";
import Image from "../image";
import Lockup from "../lockup";
const LaunchCard = styled.div`
  display: flex;
  flex-direction: column-reverse;
  background-color: #f0f8ff;

  margin-bottom: 30px;
  // margin-left: 20px;
  width: 100%;
  @media ${device.laptop} {
    width: 30%;
  }
`;

const ImagContainer = styled.div`
  padding: 40px 20px;
  background-color: #2897b1;
  position: relative;
  margin-top: auto;

  img {
    height: 100px;
    width: auto;
    display: block;
    margin: 0 auto;
  }
`;
const Content = styled.div`
  padding: 20px;
`;

function Launchcard(props) {
  const [active, setActive] = useState(false);

  const changeLike = () => {
    setActive(!active);
  };

  const trim = (text) => {
    if (text && text.length > 147) {
      return text.slice(0, 147) + "...";
    }
    return text;
  };

  return (
    <LaunchCard>
      <ImagContainer>
        <Image url={props.image} />
      </ImagContainer>

      <Content>
        <Lockup text={trim(props.description)} tag="h3" title={props.title} />
        {props.viewMore && (
          <Button size="sm" name="viewMore" text="View More" id={props.id} />
        )}
      </Content>
    </LaunchCard>
  );
}

Launchcard.propTypes = {
  image: PropTypes.string,
  description: PropTypes.string,
  title: PropTypes.string,
};

export default Launchcard;
