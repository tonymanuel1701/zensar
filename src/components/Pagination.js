import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import { makeStyles } from "@material-ui/core/styles";
import { TablePagination } from "@material-ui/core";
import styled from "styled-components";
import PropTypes from "prop-types";

const PaginationWrapper = styled.div`
  flex-shrink: 0;
`;

const CustomPagination = (props) => {
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <PaginationWrapper>
      <IconButton onClick={handleFirstPageButtonClick} disabled={page === 0}>
        <FirstPageIcon />
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0}>
        <KeyboardArrowLeft />
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
      >
        <KeyboardArrowRight />
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
      >
        <LastPageIcon />
      </IconButton>
    </PaginationWrapper>
  );
};

const Pagination = ({ pageDetails = {}, pageHandler }) => {
  return (
    <TablePagination
      component="div"
      rowsPerPageOptions={[10, 50, 100]}
      colSpan={5}
      count={pageDetails.totalDocs}
      rowsPerPage={pageDetails.limit}
      page={pageDetails.page - 1}
      onChangePage={(e, value) => pageHandler("page", value + 1)}
      onChangeRowsPerPage={(e) => pageHandler("limit", e.target.value)}
      ActionsComponent={CustomPagination}
    />
  );
};

Pagination.propTypes = {
  pageDetails: PropTypes.shape({
    totalDocs: PropTypes.number.isRequired,
    page: PropTypes.number.isRequired,
    limit: PropTypes.number.isRequired,
  }).isRequired,
  pageHandler: PropTypes.func.isRequired,
};
export default Pagination;
