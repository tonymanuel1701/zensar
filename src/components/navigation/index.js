import * as React from "react";
import styled from "styled-components";
import { device } from "../../helpers";
import theme from "../../variables";
import Burger, { BurgerMenu } from "./burger";
import NavItem from "./nav-item";

const navData = [
  { home: "/" },
  { launches: "launches" },
  { rockets: "rockets" },
];

const NavList = styled.ul`
  color: white;
  display: none;

  @media ${device.laptop} {
    display: inline-flex;
  }
`;

const NavWrapper = styled.nav`
  margin-left: auto;

  ${BurgerMenu} {
    @media ${device.laptop} {
      display: none;
    }
  }
`;

const navigation = () => {
  return (
    <>
      <NavWrapper>
        <NavList>
          {navData.map((value, index) => {
            const title = Object.keys(value)[0];
            const url = value[title];

            return (
              <NavItem
                key={index}
                title={title}
                url={url}
                color={theme.white}
              />
            );
          })}
        </NavList>
        <Burger />
      </NavWrapper>
    </>
  );
};

export default navigation;
