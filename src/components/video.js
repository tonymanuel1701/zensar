import React from "react";

const Video = ({ videoId }) => {
  const src = "https://www.youtube.com/embed/" + videoId + "?autoplay=0&mute=0";
  return <iframe width="100%" height="390" src={src}></iframe>;
};

export default Video;
