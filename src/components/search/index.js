import styled from "styled-components";
import Button from "../button";

const SearchInput = styled.input`
  border: 2px solid gray;
  border-right: 0;
  outline: none;
  border-radius: 3px;
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
  height: 26px;
  padding: 5px;
`;

const Search = ({
  inputHandler = () => {},
  clickHandler = () => {},
  value = "",
  placeHolder = "Search",
}) => {
  return (
    <div>
      <SearchInput
        onChange={inputHandler}
        placeholder={placeHolder}
        value={value}
      />
      <Button clickHandler={clickHandler} />
    </div>
  );
};
export default Search;
