import styled from "styled-components";

const Primary = styled.button`
  background: #0aad8d;
  cursor: pointer;
  color: #fff;
  height: 40px;
  padding: 5px;
  margin: 0px !important;
  outline: none !important;
`;

const ButtonSmall = styled.button`
  padding: 8px;
  outline: none;
  border: none;
  border-radius: 5px;
  color: #fff;
  background: #0aad8d;
  font-size: small;
  font-weight: bold;
  cursor: pointer;
`;

export const Button = ({
  text = "Search",
  clickHandler = () => {},
  size = "md",
  name = "",
  id = "",
}) => {
  return (
    <>
      {size == "md" && (
        <Primary onClick={clickHandler} id={id} name={name}>
          {text}
        </Primary>
      )}
      {size == "sm" && (
        <ButtonSmall onClick={clickHandler} id={id} name={name}>
          {text}
        </ButtonSmall>
      )}
    </>
  );
};

export default Button;
