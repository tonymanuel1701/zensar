import { BrowserRouter, Route, Switch } from "react-router-dom";
import styled from "styled-components";
import "./App.css";
import Hero from "./components/hero";
import Header from "./components/shared/header";
import Section from "./layout/section";
import Launches from "./pages/launches";
import Rockets from "./pages/rockets";

const MainWrapper = styled.main`
  display: block;
  position: relative;
  width: 100%;
`;

function App() {
  return (
    <MainWrapper>
      <Header />
      <Section>
        <Hero />
      </Section>
      <BrowserRouter>
        <Switch>
          <Route exact path="/launches" component={Launches} />
          <Route exact path="/rockets" component={Rockets} />
          <Route exact path="/" component={Launches} />
        </Switch>
      </BrowserRouter>
    </MainWrapper>
  );
}

export default App;
