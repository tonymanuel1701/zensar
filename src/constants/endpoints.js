export const LAUNCHES = "/launches";
export const LAUNCHES_BY_ID = "/launches/id";

export const ROCKETS = "/rockets";
