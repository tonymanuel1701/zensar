const axios = require("axios");

module.exports = {
  launches: async (req) => {
    return await axios.post(
      "https://api.spacexdata.com/v4/launches/query",
      req.body
    );
  },
  launchesById: async (req) => {
    return await axios.get(
      "https://api.spacexdata.com/v4/launches/" + req.query.id
    );
  },
  rockets: async () => {
    return await axios.get("https://api.spacexdata.com/v4/rockets");
  },
};
